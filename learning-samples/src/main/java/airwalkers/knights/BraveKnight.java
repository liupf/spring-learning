package airwalkers.knights;

/**
 * Created by liupf on 2017/5/16.
 * 将接口注入到依赖方类而不是 自行依赖 或 注入具体类，实现了松耦合
 */
public class BraveKnight implements Knight {
    private Quest quest;

    public BraveKnight(Quest quest) {
        this.quest = quest;
    }

    public void embarkOnQuest() {
        quest.embark();
    }
}
