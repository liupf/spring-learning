package airwalkers.knights;

/**
 * Created by liupf on 2017/5/16.
 */
public interface Knight {
    void embarkOnQuest();
}
