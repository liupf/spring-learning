package airwalkers.knights;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by liupf on 2017/5/16.
 * 使用xml装配类
 */
public class KnightMain {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("knight.xml");
        Knight knight = context.getBean(Knight.class);
        knight.embarkOnQuest();
        context.close();
    }
}
