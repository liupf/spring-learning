package airwalkers.knights;

import java.io.PrintStream;

/**
 * Created by liupf on 2017/5/16.
 * 类似于Knight，这里实现了松耦合的依赖注入
 * 微领队C#依赖注入原理及在于此
 */
public class SlayDragonQuest implements Quest{
    private PrintStream stream;

    public SlayDragonQuest(PrintStream stream) {
        this.stream = stream;
    }

    public void embark() {
        stream.println("slay dragon.");
    }
}
