package airwalkers.knights;

/**
 * Created by liupf on 2017/5/15.
 * 自己的理解来 “装配“ 组件
 */
public class KnightMain_byMyself {
    public static void main(String[] args) {
        Quest quest = new SlayDragonQuest(System.out);
        Knight knight = new BraveKnight(quest);
        knight.embarkOnQuest();
    }
}
